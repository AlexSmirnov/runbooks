## Steps to check

1. Check network, try to open [GitLab.com](https://gitlab.com). If it is ok from your side, then it can be only network failure.
1. Check the [fleet overview](on http://dashboards.gitlab.net/dashboard/db/fleet-overview).
1. Check the [database load](http://dashboards.gitlab.net/dashboard/db/postgres-stats).
